package org.test.controller;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.test.entity.Test;
import org.test.service.DemoService;
import org.test.service.ITestService;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 * 文件表 前端控制器
 * </p>
 *
 * @author funkye
 * @since 2019-03-20
 */
@RestController
@RequestMapping("/test")
public class TestController {
    @Reference(version = "1.0.0", timeout = 60000)
    @Lazy
    private ITestService testService;
    private final static Logger logger = LoggerFactory.getLogger(TestController.class);
    @Autowired
    @Lazy
    DemoService demoService;
    private Lock lock = new ReentrantLock();

    /**
     * 测试分页结果是否正确
     * 
     * @return
     */
    @GetMapping(value = "testPage")
    public Object testPage() {
        Page<Test> page = new Page<Test>(1, 10);
        return testService.page(page);
    }

    /**
     * 测试异常回滚分布式事务接口
     * 
     * @return
     */
    @GetMapping(value = "seataCommit")
    public Object seataCommit() {
        return demoService.seataCommit();
    }

    /**
     * 测试本地事务接口
     * 
     * @return
     */
    @GetMapping(value = "commit")
    public Object commit() {
        return demoService.commit();
    }
}
